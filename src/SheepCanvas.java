import com.nttdocomo.ui.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Random;
import javax.microedition.io.Connector;

/**
 * Sheep Canvas.
 *
 * history:
 *  8, July 2004 - add resume event.
 * @author takkie
 */
class SheepCanvas extends Canvas implements Runnable {

  /*
  *
  */
  private static int i, j, k;

  /*
  *
  */
  private static int l, m, n;

  /**
  * Thread.
  */
  private Thread runner;
  
  /**
  * create random number.
  */
  private static Random rand;

  /**
  * the system font type.
  */
  private static Font font;

  /**
  * the sheep action.
  */
  private static int action = 0;

  /**
  * the application state.
  */
  private static int state;

  /**
  * the sheep number.
  */
  private static int sheep_number;

  /**
  * the sheep image.
  */
  private Image sheep_image[];

  /**
  * the frame image.
  */
  private Image background;

  /**
  * the sheep position.
  */
  private static int sheep_pos[][] = new int[100][3];

  /**
  * key flag.
  */
  private static int key_flag;

  private String serial = "";

  /**
  * do nothing.
  */
  public SheepCanvas() {
  }

  /**
  * start application.
  */
  public void start() {

    // set serial number;
    serial = "serial:" + IApplication.getCurrentApp().getArgs()[0];

    /* create object */
    state = 1;
    rand = new Random();
    font = Font.getDefaultFont();

    sheep_image = new Image[2];

    /* load image */
    try {
      for(i = 0; i < 2; i++) {
        MediaImage mi = MediaManager.getImage
            ("resource:///sheep0" + i + ".gif");
        mi.use();
        sheep_image[i] = mi.getImage();
      }

      MediaImage mi = MediaManager.getImage
          ("resource:///background.gif");
      mi.use();
      background = mi.getImage();

      DataInputStream din = null;
      
      din = Connector.openDataInputStream("scratchpad:///0;pos=0");

      sheep_number = din.readInt();

      din.close();
      din = null;

    } catch(Exception exception) {
        IApplication.getCurrentApp().terminate();
    }

    /* set the sheep position */
    for(i = 0; i < sheep_pos.length; i++) {
        sheep_pos[i][0] = 0;
        sheep_pos[i][1] = -1;
        sheep_pos[i][2] = 0;
    }

    sheep_pos[0][0] = (Display.getWidth() - Display.getWidth() % 10) + 10;
    sheep_pos[0][1] = (Display.getHeight() - 40) + rand.nextInt() % 30;
    sheep_pos[0][2] = 0;

    /* set soft key label */
    setSoftLabel(1, "ZZ");

    state = 5;

    /* start running */
    runner = new Thread(this);
    runner.start();

  }

  /**
  * run method.
  */
  public void run() {

    while(true) {

      calc();
      repaint();

      try {
          Thread.sleep(100);
      } catch(Exception exception) {
      }

    }

  }

  /**
  * calculation method.
  */
  private void calc() {

    if(state == 5) {

      /* data save and fin application */ 
      if(isKeyPressed(Display.KEY_SOFT2)) {

        try {
            DataOutputStream din = null;
            
            din = Connector.openDataOutputStream
                ("scratchpad:///0;pos=0");
            din.writeInt(sheep_number);
            din.close();
            din = null;
        }
        catch(Exception exception) {
        }
        IApplication.getCurrentApp().terminate();

      }
        
      /* run the sheep */
      else if(isKeyPressed(Display.KEY_SELECT)) {

        /* add a new sheep */
        for(i = 1; i < sheep_pos.length; i++) {

          if(sheep_pos[i][1] == -1) {

            sheep_pos[i][0] = (Display.getWidth() - Display.getWidth() % 10) + 10;
            sheep_pos[i][1] = (Display.getHeight() - 40) + rand.nextInt() % 30;
            sheep_pos[i][2] = 0;

            break;

          }

        }

      }

      /* run the sheep */
      for(i = 0; i < sheep_pos.length; i++) {
        if(sheep_pos[i][1] >= 0) {
          sheep_pos[i][0] -= 5;

          /* run */
          if(sheep_pos[i][0] > 50 && sheep_pos[i][0] <= 70) {
            sheep_pos[i][1] -= 3;
          }
          
          /* jump */
          else {
            if(sheep_pos[i][0] > 30 && sheep_pos[i][0] <= 50) {
                sheep_pos[i][1] += 3;
            }
            if(sheep_pos[i][0] < 60 && sheep_pos[i][2] == 0) {
                sheep_number++;
                sheep_pos[i][2] = 1;
            }
          }

          /* remove a frameouted sheep */
          if(sheep_pos[i][0] < -20) {
            if(i == 0) {
                sheep_pos[0][0] = (Display.getWidth() - Display.getWidth() % 10) + 10;
                sheep_pos[0][2] = 0;
            } else {
                sheep_pos[i][0] = 0;
                sheep_pos[i][1] = -1;
                sheep_pos[i][2] = 0;
            }
          }

        }

      }

      action = 1 - action;

    }

    else {
      if(state != 6);
    }
  }

  public void paint(Graphics g) {
      g.lock();

      if(state == 5) {
          g.setColor(Graphics.getColorOfRGB(100, 255, 100));
          g.fillRect(0, 0, Display.getWidth(), Display.getHeight());
          g.setColor(Graphics.getColorOfRGB(150, 150, 255));
          g.fillRect(0, 0, Display.getWidth(), (Display.getHeight() - background.getHeight()) + 10);
          g.drawImage(background, 35, Display.getHeight() - background.getHeight());
          for(l = 0; l < sheep_pos.length; l++) {
              if(sheep_pos[l][1] >= 0) {
                  g.drawImage(sheep_image[action], sheep_pos[l][0], sheep_pos[l][1]);
              }

          }

          g.setColor(Graphics.getColorOfName(0));
          String s = "number: " + sheep_number;
          g.drawString(s, Display.getWidth() - font.stringWidth(s) - 5, 15);

          // draw a serial number
          s = serial;
          g.drawString(s, Display.getWidth() - font.stringWidth(s) - 5, 30);

      }

      g.unlock(true);

  }

  private boolean isKeyPressed(int key)
  {
      return (getKeypadState() & (1 << key)) != 0;
  }

}
