@ECHO OFF

REM =======================================
REM
REM    make.bat  ver. 1.6
REM
REM =======================================
REM
REM [Purpose]
REM   DoCoMo appli compile
REM
REM [Tag List]
REM   ALL        : make appli file
REM   PROGUARD   : make appli file with "proguard.jar"
REM   RETROGUARD : make appli file with "retroguard.jar"
REM   BASE       : compile only
REM   PRE        : preverify many classes
REM   PRO        : excute proguard
REM   RETRO      : excute retroguard 
REM   JAR        : jar compress
REM   DOC        : make the api document
REM   JAM        : write a jad file
REM   CLAEN      : clean many dist files
REM   HTML       : output a download html file
REM
REM [Derectory]
REM   src        : source files
REM   res        : resource file
REM   classes    : class files
REM   bin        : binary file
REM
REM =======================================


REM =======================
REM    SET ENVIROMENT

SET PROJECT=sheep

SET JAR=%PROJECT%.jar
SET JAM=%PROJECT%.jam

SET DL=index.html

SET APPLI=%PROJECT%
SET CLASS=Sheep
SET SPSIZE=32
SET NETWORK=
SET PARAM=0

REM SET JAVA_HOME=c:\java\jdk1.1.8
SET BOOT=C:\iDK15\lib\classes.zip
SET LIB=C:\iDK15\lib\doja_classes.zip
SET PREVERIFY=C:\iDK15\bin\preverify.exe

SET PROGUARD=C:\temp\proguard.jar
SET RETROGUARD=C:\temp\retroguard.jar

REM =======================


REM =======================
REM    SELECT ACTION

IF "%1" == "" GOTO ALL
GOTO %1

GOTO :EOF

REM =======================


REM =======================
REM    MAKE BINARY FILE

:ALL

CALL :BASE
CALL :PRE
CALL :JAR
CALL :JAM

GOTO :EOF

REM =======================


REM =======================
REM    MAKE BINARY FILE WITH PROGUARD

:PROGUARD

CALL :BASE
CALL :PRO
CALL :PRE
CALL :JAR
CALL :JAM

GOTO :EOF

REM =======================


REM =======================
REM    MAKE BINARY FILE WITH RETROGUARD

:RETROGUARD

CALL :BASE
CALL :RETRO
CALL :PRE
CALL :JAR
CALL :JAM

GOTO :EOF

REM =======================


REM =======================
REM    COMPILE

:BASE

IF NOT EXIST classes MKDIR classes
%JAVA_HOME%\bin\javac -g:none -d classes -bootclasspath %BOOT% -classpath %LIB% src\*.java

GOTO :EOF

REM =======================


REM =======================
REM    PREVERIFY 

:PRE

%PREVERIFY% -d classes -classpath %BOOT%;%LIB%;. classes

GOTO :EOF

REM =======================


REM =======================
REM    PROGUARD

:PRO

IF NOT EXIST bin\%JAR% (
  ECHO Warning: There is not many dist files.
  ECHO          You have to build by default target^(none option^).

  GOTO :EOF
)

%JAVA_HOME%\bin\jar cvfM in.jar -C classes .
DEL /Q classes\*
%JAVA_HOME%\bin\java -jar %PROGUARD% -injar in.jar -outjar out.jar -libraryjars %LIB%;%BOOT% -keep class %CLASS% { public void start(); }
DEL in.jar
MOVE out.jar classes
CD classes
%JAVA_HOME%\bin\jar xf out.jar
DEL out.jar
CD ..

GOTO :EOF

REM =======================


REM =======================
REM    RETROGUARD

:RETRO

IF NOT EXIST bin\%JAR% (
  ECHO Warning: There is not many dist files.
  ECHO          You have to build by default target^(none option^).

  GOTO :EOF
)

%JAVA_HOME%\bin\jar cvfM in.jar -C classes .
DEL /Q classes\*
%JAVA_HOME%\bin\java -classpath %LIB%;%RETROGUARD% RetroGuard
DEL in.jar
MOVE out.jar classes
CD classes
%JAVA_HOME%\bin\jar xf out.jar
DEL out.jar
RMDIR /S/Q META-INF
CD ..

GOTO :EOF

REM =======================


REM =======================
REM    JAR COMPRESS

:JAR

IF NOT EXIST classes (

  ECHO Warning: Many class files don't exist.
  ECHO          You have to build by default target^(none option^).

  GOTO :EOF

)

IF NOT EXIST bin MKDIR bin

%JAVA_HOME%\bin\jar -cvfM bin\%JAR% -C classes .
IF EXIST res %JAVA_HOME%\bin\jar -uvfM bin\%JAR% -C res .

GOTO :EOF

REM =======================


REM =======================
REM    MAKE JAVADOC

:DOC

IF NOT EXIST doc MKDIR doc
%JAVA_HOME%\bin\javadoc -d doc -classpath %BOOT%;%LIB% -private -author -version src\*.java

GOTO :EOF

REM =======================


REM =======================
REM    MAKE JAM FILE

:JAM

IF NOT EXIST bin\%JAR% (

  ECHO Warning: A target dist jar file is not exist!!
  ECHO          You have to build by JAR target^(or none option^).

  GOTO :EOF

)

FOR %%A IN (bin\%JAR%) DO SET SIZE=%%~zA

IF "%DATE:~5,2%" == "01" SET MONTH=Jan
IF "%DATE:~5,2%" == "02" SET MONTH=Feb
IF "%DATE:~5,2%" == "03" SET MONTH=Mar
IF "%DATE:~5,2%" == "04" SET MONTH=Apr
IF "%DATE:~5,2%" == "05" SET MONTH=May
IF "%DATE:~5,2%" == "06" SET MONTH=Jun
IF "%DATE:~5,2%" == "07" SET MONTH=Jul
IF "%DATE:~5,2%" == "08" SET MONTH=Aug
IF "%DATE:~5,2%" == "09" SET MONTH=Sep
IF "%DATE:~5,2%" == "10" SET MONTH=Oct
IF "%DATE:~5,2%" == "11" SET MONTH=Nov
IF "%DATE:~5,2%" == "12" SET MONTH=Dec

ECHO PackageURL = %JAR%> bin\%JAM%
ECHO AppName = %APPLI%>> bin\%JAM%
ECHO AppClass = %CLASS%>> bin\%JAM%
ECHO AppSize = %SIZE%>> bin\%JAM%
ECHO LastModified = Fri, %DATE:~8,2% %MONTH% %DATE:~0,4% %TIME:~0,2%:%TIME:~3,2%:00>> bin\%JAM%
IF NOT "%NETWORK%" == "" ECHO UseNetwork = %NETWORK%>> bin\%JAM%
IF NOT "%SPSIZE%" == "" ECHO SPSize = %SPSIZE% >> bin\%JAM%
IF NOT "%PARAM%" == "" ECHO AppParam = %PARAM% >> bin\%JAM%

GOTO :EOF

REM =======================


REM =======================
REM    CLEAN FILES

:CLEAN

IF EXIST bin RMDIR/S/Q bin
IF EXIST classes RMDIR/S/Q classes

GOTO :EOF

REM =======================


REM =======================
REM    MAKE HTML FILE

:HTML

IF NOT EXIST bin\%JAR% (
  ECHO Warning: There is not many dist files.
  ECHO          You have to build by default target^(none option^).
)
IF NOT EXIST bin\%JAM% (
  ECHO Warning: There is not many dist files.
  ECHO          You have to build by default target^(none option^).
)

ECHO ^<OBJECT declare id="%APPLI%"> %DL%
ECHO   data="bin/%JAM%">> %DL%
ECHO   type="application/x-jam"^>>> %DL%
ECHO ^</OBJECT^>>> %DL%
ECHO ^<A ijam="#%APPLI%" href="error.html"^>%APPLI%^</A^>>> %DL%

GOTO :EOF

REM =======================


